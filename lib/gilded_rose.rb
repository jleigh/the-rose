class GildedRose
  attr_reader :items

  BRIE = "Aged Brie"
  PASS = "Backstage passes to a TAFKAL80ETC concert"
  SULFURAS = "Sulfuras, Hand of Ragnaros"
  MAX_QUALITY = 50
  MIN_QUALITY = 0
  EXPIRY = 0

  def initialize(items)
    @items = items
  end

  def update
    items.each do |item|
      update_quality(item)
      update_sell_in(item)
    end
  end

  private

  def update_quality(item)
    if decreases_with_age?(item)
      item.quality -= 1 if above_min?(item)

    else
      if under_max?(item)
        item.quality += 1
        handle_pass(item) if item.name == PASS
      end
    end
  end

  def update_sell_in(item)
    item.sell_in -= 1 if item.name != SULFURAS
  end

  def decreases_with_age?(item)
    item.name != BRIE && item.name != PASS && item.name != SULFURAS
  end

  def under_max?(item)
    item.quality < MAX_QUALITY
  end

  def above_min?(item)
    item.quality > MIN_QUALITY
  end

  def handle_pass(item)
    if item.sell_in < EXPIRY
      item.quality -= item.quality 
    else
      item.quality += 1 if item.sell_in < 11
      item.quality += 1 if item.sell_in < 6
    end
  end
end
