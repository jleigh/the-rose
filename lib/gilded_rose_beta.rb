class GildedRoseBeta
  attr_reader :items, :mapper

  def initialize(items)
    @items = items
    @mapper = RuleMapper
  end

  def update
    items.each do |item|
      rules = mapper.map(item.name)
      updater = ItemWrapper.new(item, rules)
      updater.update
    end
  end
end