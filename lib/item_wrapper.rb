class ItemWrapper
	attr_reader :item, :rules
	def initialize(item, rules)
		@item = item
		@rules = rules
	end

	def update
		return if rules.empty?
    rules.each do |rule|
      next unless rule.active?(item.sell_in)
      item.quality = calc_quality(rule, item)
    end

    item.sell_in -= 1
  end

  private

  def calc_quality(rule, item)
    quality = rule.delta + item.quality

    if quality < rule.min_quality
      rule.min_quality
    elsif quality > rule.max_quality
      rule.max_quality
    else
      quality 
    end
  end
end