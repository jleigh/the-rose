class Rule
	attr_reader :delta, :sell_in_start, :sell_in_end, :min_quality, :max_quality
	
	def initialize(delta, sell_in_start, sell_in_end, min_quality = 0, max_quality = 50)
		@delta = delta
		@sell_in_start, @sell_in_end = sell_in_start, sell_in_end
		@min_quality, @max_quality = min_quality, max_quality
	end

	def active?(sell_in)
		sell_in <= sell_in_start && sell_in >= sell_in_end
	end
end