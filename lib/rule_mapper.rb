class RuleMapper

  def self.map(item)
    mapping[item] ? mapping[item] : mapping["Default"]
  end

	def self.mapping
		{ 
      "Backstage passes to a TAFKAL80ETC concert" => [
        Rule.new(delta=1, sell_in_start=Float::INFINITY, sell_in_end=11),
        Rule.new(delta=2, sell_in_start=10, sell_in_end=6),
        Rule.new(delta=3, sell_in_start=5, sell_in_end=0),
        Rule.new(delta=-Float::INFINITY, sell_in_start=-1, sell_in_end=-Float::INFINITY) # set to 0 rule
      ],
      "Sulfuras, Hand of Ragnaros" => [],
      "Aged Brie" => [
        Rule.new(delta=1, sell_in_start=Float::INFINITY, sell_in_end=-Float::INFINITY)
      ],
      "Conjured Mana Cakes" => [
        Rule.new(delta=-2, sell_in_start=Float::INFINITY, sell_in_end=0),
        Rule.new(delta=-4, sell_in_start=-1, sell_in_end=-Float::INFINITY) # decrease twice as fast as first
      ],
      "Default" => [
        Rule.new(delta=-1, sell_in_start=Float::INFINITY, sell_in_end=0),
        Rule.new(delta=-2, sell_in_start=0, sell_in_end=-Float::INFINITY)
      ]
		}
	end 

  private_class_method :mapping
end