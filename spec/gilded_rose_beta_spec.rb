require "spec_helper"

describe GildedRoseBeta do
  let(:items) { [Item.new("Regular", 10, 3)] }
  let(:gilded_rose) { GildedRoseBeta.new(items) }

  describe "#initialize" do
    it "initializes with items and a mapping" do
      expect(gilded_rose.items).to eq(items)
      expect(gilded_rose.mapper).to eq(RuleMapper)
    end
  end

  describe "#update" do
    it "calls item updater and mapper" do
      expect(RuleMapper).to receive(:map).with(items[0].name)
      expect_any_instance_of(ItemWrapper).to receive(:update)
      gilded_rose.update
    end
  end
end