require "spec_helper"

describe GildedRose do
  let(:items) { [Item.new("foo", 1, 1), Item.new("foo", 0, 0)] }
  let(:rose) { GildedRose.new(items) }

  describe "initialize" do
    it "initializes with items" do
      expect(rose.items).to eq(items)
    end
  end

  describe "#update" do
    before do
      rose.update
    end

    it "does not change the name" do
      item = items[0]
      expect(item.name).to eq("foo")
    end

    it "quality and sell_in decreases by 1 daily" do
      item = items[0]
      expect(item.quality).to eq(0)
      expect(item.sell_in).to eq(0)
    end

    it "quality is never negative" do
      item = items[1]
      expect(item.quality).to eq(0)
      expect(item.sell_in).to eq(-1)
    end

    context "Aged Brie" do
      let(:items) { [Item.new("Aged Brie", 10, 5), Item.new("Aged Brie", 10, 50)] }

      it "quality increases as it ages" do
        item = items[0]
        expect(item.sell_in).to eq(9)
        expect(item.quality).to eq(6)
      end

      it "quality is never above 50" do
        item = items[1]
        expect(item.sell_in).to eq(9)
        expect(item.quality).to eq(50)
      end
    end

    context "Sulfuras" do
      let(:items) { [Item.new("Sulfuras, Hand of Ragnaros", 10, 80)] }

      it "does not change in sell_in or quality" do
        item = items[0]
        expect(item.sell_in).to eq(10)
        expect(item.quality).to eq(80)
      end
    end

    context "Backstage pass" do
      let(:item1) { Item.new("Backstage passes to a TAFKAL80ETC concert", 11, 10) }
      let(:item2) { Item.new("Backstage passes to a TAFKAL80ETC concert", 10, 10) }
      let(:item3) { Item.new("Backstage passes to a TAFKAL80ETC concert", 6, 10) }
      let(:item4) { Item.new("Backstage passes to a TAFKAL80ETC concert", 5, 10) }
      let(:item5) { Item.new("Backstage passes to a TAFKAL80ETC concert", 1, 10) }
      let(:item6) { Item.new("Backstage passes to a TAFKAL80ETC concert", -1, 10) }
      let(:item7) { Item.new("Backstage passes to a TAFKAL80ETC concert",11, 50) }
      let(:items) { [item1, item2, item3, item4, item5, item6, item7] }

      it "never sets quality above 50" do
        item = items[6]
        expect(item.sell_in).to eq(10)
        expect(item.quality).to eq(50)
      end

      it "when sellin <= 11 quality increases by 1" do
        item = items[0]
        expect(item.sell_in).to eq(10)
        expect(item.quality).to eq(11)
      end

      it "when sellin > 11 and < 5 quality increases by 2" do
        item1 = items[1]
        item2 = items[2]
        expect(item1.sell_in).to eq(9)
        expect(item1.quality).to eq(12)
        expect(item2.sell_in).to eq(5)
        expect(item2.quality).to eq(12)
      end

      it "when sellin < 6 and > -1 quality increases by 3" do
        item1 = items[3]
        item2 = items[4]
        expect(item1.sell_in).to eq(4)
        expect(item1.quality).to eq(13)
        expect(item2.sell_in).to eq(0)
        expect(item2.quality).to eq(13)
      end

      it "when sellin >= 0 quality set to 0" do
        item = items[5]
        expect(item.sell_in).to eq(-2)
        expect(item.quality).to eq(0)
      end
    end
  end
end
