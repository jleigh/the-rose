require "spec_helper"

describe Item do
  let(:item) { Item.new("foo", 0, 0) }

  describe "#initialize" do
    it "assigns name, sell_in and quality" do
      expect(item.name).to eq("foo")
      expect(item.sell_in).to eq(0)
      expect(item.quality).to eq(0)
    end
  end

  describe "#to_s" do
    it "returns attributes as a string" do
        expect(item.to_s).to eq("foo, 0, 0")
    end
  end
end
