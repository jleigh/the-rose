require "spec_helper"

describe ItemWrapper do
	
	let(:item) { Item.new("Regular", 10, 3) }
	let(:rules) { [] }
	let(:wrapper) { ItemWrapper.new(item, rules) }

 	describe "#initialize" do
		it "assigns items and rules" do
			expect(wrapper.item).to eq(item)
			expect(wrapper.rules).to eq(rules)
		end
	end

	describe "#update" do
		before do
			wrapper.update
		end

		it "does not update name" do
      		expect(item.name).to eq("Regular")
    	end

    	context "without rules" do
			it "does not update item" do
				expect(item.quality).to eq(3)
				expect(item.sell_in).to eq(10)
			end
		end

		context "with rules" do
			let(:rules) { RuleMapper.map("Regular") }

		    context "when quality is 0" do
		      	let(:item) { Item.new("Regular", 3, 0) }

			      it "never goes < 0" do
			        	expect(item.sell_in).to eq(2)
			        	expect(item.quality).to eq(0)
			      end
		    end
    
		    context "when quality is 50" do
		      	let(:item) { Item.new("Aged Brie", 1, 50) }
		      	let(:rules) { RuleMapper.map("Aged Brie") }

		      	it "never goes > 50" do
		        	expect(item.sell_in).to eq(0)
		        	expect(item.quality).to eq(50)
		      	end
		    end

		    context "when sell_in has not passed" do
		      	let(:item) { Item.new("Regular", 1, 3) }

		      	it "decrements sell_in and quality by 1" do
		        	expect(item.sell_in).to eq(0)
		        	expect(item.quality).to eq(2)
		      	end
		    end

		    context "when sell_in has passed" do
		      	let(:item) { Item.new("Regular", -1, 3) }

		      	it "decrements sell_in by 1 and quality by 2" do
		        	expect(item.sell_in).to eq(-2)
		        	expect(item.quality).to eq(1)
		      	end
		    end

		    context "Aged Brie" do
			    let(:item) { Item.new("Aged Brie", 1, 3) }
			    let(:rules) { RuleMapper.map("Aged Brie") }

			    it "increments quality by 1 and decrements sell_in by 1" do
			       	expect(item.sell_in).to eq(0)
			       	expect(item.quality).to eq(4)
			    end
			end

			context "Conjured" do
			    let(:rules) { RuleMapper.map("Conjured Mana Cakes") }

			    context "when sell_in has not passed" do
			    	let(:item) { Item.new("Conjured Mana Cakes", 10, 15) }
			    	it "decrements quality by 2 and sell_in by 1" do
				        expect(item.sell_in).to eq(9)
				        expect(item.quality).to eq(13)
			    	end
			    end
			    
			    context "when sell_in has passed" do
			    	let(:item) { Item.new("Conjured Mana Cakes", -1, 15) }

			    	it "decrements quality by 4 and sell_in by 1" do
		        		expect(item.sell_in).to eq(-2)
		        		expect(item.quality).to eq(11)
		      		end
			    end
			end

			context "Sulfuras" do
		      	let(:item) { Item.new("Sulfuras, Hand of Ragnaros", 10, 80) }
		      	let(:rules) { RuleMapper.map("Sulfuras, Hand of Ragnaros") }

		      	it "does not change" do
		        	expect(item.sell_in).to eq(10)
		        	expect(item.quality).to eq(80)
		      	end
		    end

		    context "Backstage pass" do
				let(:rules) { RuleMapper.map("Backstage passes to a TAFKAL80ETC concert") }

				context "when sell_in is > 10" do
		       		let(:item) { Item.new("Backstage passes to a TAFKAL80ETC concert", 11, 15) }
		       		it "quality increases by 1" do
		         		expect(item.sell_in).to eq(10)
		         		expect(item.quality).to eq(16)
		       		end
		     	end

		     	context "when sell_in is between 11 and 5" do
		        	let(:item) { Item.new("Backstage passes to a TAFKAL80ETC concert", 10, 15) }
		        	it "quality increases by 2" do
		          		expect(item.sell_in).to eq(9)
		          		expect(item.quality).to eq(17)
		        	end
		      	end

		      	context "when sell_in is between 6 and -1" do
		        	let(:item) { Item.new("Backstage passes to a TAFKAL80ETC concert", 5, 15) }
		        	it "quality increases by 3" do
		          		expect(item.sell_in).to eq(4)
		          		expect(item.quality).to eq(18)
		        	end
		      	end

		      	context "when sell_in is < 0" do
	    			let(:item) { Item.new("Backstage passes to a TAFKAL80ETC concert", -1, 15) }
			        it "quality flatlines to 0" do
			          expect(item.sell_in).to eq(-2)
			          expect(item.quality).to eq(0)
			        end
			    end
			end

		end
    end
end