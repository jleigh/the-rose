require "spec_helper"

describe RuleMapper do
	describe ".map" do
		let(:mapping) { {"fizz" => "Rule 1", "Default" => "Rule 2"} }

		before do
			allow(RuleMapper).to receive(:mapping).and_return(mapping)
		end

		context "when item is not in the map" do
			it "returns the default rule" do
				expect(RuleMapper.map("foo")).to eq("Rule 2")
			end
		end

		context "when item is in the map" do
			it "applies the correct rule" do
				expect(RuleMapper.map("fizz")).to eq("Rule 1")
			end
		end
	end
end
