require "spec_helper"

describe Rule do
	let(:rule) { Rule.new(delta=-1, sell_in_start=Float::INFINITY, sell_in_end=0) }

	describe "#initialize" do
		it "assigns field, operator, value" do
			expect(rule.delta).to eq(-1)
			expect(rule.sell_in_start).to eq(Float::INFINITY)
			expect(rule.sell_in_end).to eq(0)
			expect(rule.min_quality).to eq(0)
			expect(rule.max_quality).to eq(50)
		end
	end

	describe "#active?" do
		it "returns true when given sell_in is within its sell_in_start and sell_in_end" do
			expect(rule.active?(10)).to eq(true)
		end

		it "returns false when sell_in is not within its sell_in_start and sell_in_end" do
			expect(rule.active?(-2)).to eq(false)
		end
	end
end
